import React, { useEffect, useState } from 'react';
import {
    Link
} from 'react-router-dom';

import AppContainer from './AppContainer';

import api from '../api';

const Home = () => {
    const [posts, setPosts] = useState(null);

    const fetchPosts = () => {
        api.getAllPosts().then(res => {
            const result = res.data.data;
            setPosts(result);
        });
    };

    useEffect(() => {
        fetchPosts();
    }, []);

    const renderPosts = () => {
        if (!posts) {
            return (
                <tr>
                    <td colSpan="4">
                        Loading posts...
                    </td>
                </tr>
            );
        } else if (posts.length > 0) {
            return posts.map((post) => (
                <tr key={`post${post.id}`}>
                    <td>{post.id}</td>
                    <td>{post.title}</td>
                    <td>{post.description}</td>
                    <td>
                        <Link to={`/edit/${post.id}`} className="btn btn-warning">Edit</Link>
                        <button
                            type="button"
                            className="btn btn-danger"
                            onClick={() => {
                                const deleteAlert = confirm(`Are you sure you want to delete id ${post.id}?`);
                                if (deleteAlert === true) {
                                    api.deletePost(post.id)
                                    .then(fetchPosts)
                                    .catch(err => {
                                        alert(`Failed to delete post with id ${post.id}`)
                                    });
                                }
                            }}
                        >
                            Delete
                        </button>
                    </td>
                </tr>
            ));
        }

        return (
            <tr>
                <td colSpan="4">
                    No post added yet.
                </td>
            </tr>
        );
    };

    return (
        <AppContainer
            title="Laravel & ReactJS CRUD"
        >
            <Link to="/add" className="btn btn-primary">Add Post</Link>
            <div className="table-responsive">
                <table className="table table-striped mt-4">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {renderPosts()}
                    </tbody>
                </table>
            </div>
        </AppContainer>
    );
};

export default Home;
