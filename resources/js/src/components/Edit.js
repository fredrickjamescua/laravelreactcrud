import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import AppContainer from './AppContainer';
import api from '../api';

const Edit = () => {
    const { id } = useParams();
    const history = useHistory();
    const [isLoading, setIsLoading] = useState(false);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');

    useEffect(() => {
        api.getPost(id).then(res => {
            const result = res.data.data;
            setTitle(result.title);
            setDescription(result.description);
        });
    }, []);

    const onSubmit = async () => {
        setIsLoading(true);
        try {
            await api.updatePost(id, { title, description });
            history.push('/');
        } catch {
            alert('Failed to update post.')
        } finally {
            setIsLoading(false);
        }
    };

    return (
        <AppContainer
            title="Edit Post"
        >
            <form>
                <div className="form-group">
                    <label>Title</label>
                    <input
                        className="form-control"
                        type="text"
                        value={title}
                        onChange={e => setTitle(e.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <textarea
                        className="form-control"
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                    >
                    </textarea>
                </div>
                <div className="form-group">
                    <button
                        disabled={isLoading}
                        type="button"
                        className="btn btn-success"
                        onClick={onSubmit}
                    >
                        {isLoading ? 'Saving...' : 'Edit'}
                    </button>
                </div>
            </form>
        </AppContainer>
    );
};

export default Edit;
