import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import AppContainer from './AppContainer';
import api from '../api';

const Add = () => {
    const history = useHistory();
    const [isLoading, setIsLoading] = useState(false);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');

    const onSubmit = async () => {
        setIsLoading(true);
        try {
            await api.createPost({ title, description });
            history.push('/');
        } catch {
            alert('Failed to create post.')
        } finally {
            setIsLoading(false);
        }
    };

    return (
        <AppContainer
            title="Add Post"
        >
            <form>
                <div className="form-group">
                    <label>Title</label>
                    <input
                        className="form-control"
                        type="text"
                        value={title}
                        onChange={e => setTitle(e.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <textarea
                        className="form-control"
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                    >
                    </textarea>
                </div>
                <div className="form-group">
                    <button
                        disabled={isLoading}
                        type="button"
                        className="btn btn-success"
                        onClick={onSubmit}
                    >
                        {isLoading ? 'Saving...' : 'Save'}
                    </button>
                </div>
            </form>
        </AppContainer>
    );
};

export default Add;
